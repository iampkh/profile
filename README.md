
<!-- Profile Picture section-->
<table width="650" >
</tr>
<td><!-- DP and Name column -->			
			
<div>
<h1 align="center">
<a name="profile_pic" > <img src="https://gitlab.com/uploads/-/system/user/avatar/5143742/avatar.png?width=400" style="border-radius:50%"> </a>
<br> Prakash Paramasivam
</h1>
<!-- Details section -->
<h3 align="center">
pkh.japan@gmail.com 
<br> Exp:12 yrs</br>
</h3>
</div>

</td> <!-- DP and Name column end -->
<td><!-- Thoughts -->
<div >

<h5>
Coding is fun when we see our output live in use, <br/>
Mostly I think of a script/software when I feel bored of doing the same thing again and again . <br/>
Eventualy I noticed that boring is also good too  :stuck_out_tongue_winking_eye:  . <br/>
</h5>

##### Tips 
1. Find a best suitable OpenSource technology which solves your problem.
2. Learn how to run the tech for couple of days, thats it  you are good to go.	
3. Thats it , you got everything. fun developings	

</div></div></td>
</td><!-- Thoughts end -->
</tr>
</table>

<!-- <h2 name="menu_header" align="center">
		Technical Experience
	</h2> -->

<!-- Beginning of Menu1 -->
<div>	
	<div align="center" name="menu">
		<h2>
			<a href="https://gitlab.com/iampkh/profile">
				Technical
			</a>
			<span>|</span>
			<a href="https://gitlab.com/iampkh/profile">
				Current Activities
			</a>
			<span>|</span>
			<a href="https://gitlab.com/iampkh/profile">
				Hobbies
			</a>
		</h2>
	</div>
</div>
<!-- End of Menu1 -->

<!-- Technical Experience GanttChart -->

### Technical
```mermaid

gantt
dateFormat  YYYY-MM
title Prakash's Technical Experience ...



section Beginning 
Python scripting	:done,des1,2012-01,2012-06
Java swing		:done,des2, 2012-01,2012-06  

section PalleTech <br/> Intern-Trainee
Android MobileApp (Edudroid) :done, des1, 2012-05,2012-10 
Android Game AppDev	:done, des2, 2012-07,2012-10 

section SourceEdge <br/> Junion Engineer
 Android-Mobile (TaxiMeter) 	:done, des1, 2012-10,2013-03 
 Android-Mobile (Millenium) 	:done, des2, 2013-03,2013-05
 Android-Mobile  (Enigha) 	:done, des3, 2013-05,2013-07
 Android-Mobile (TravelAlarm) 	:done, des4, 2013-05,2013-07

section Scorg <br/> Software Engineer @Deputed to HCL
HTML/JS-AndroidTV (Settings) :done, des1, 2013-08,2014-06 

section HCL (SeniorEngineer[MTS])
Android-TV (BasicSettings) :done, des1, 2014-06,2014-10 
Android-TV (TvShortCuts) :done, des2, 2014-07,2015-03
Android-TV (PictureControl) :done, des3, 2014-07,2016-09
Java-Swing(Applet) : done, des4, 2015-04,2016-08
Android-SharedLib :done,des5,2014-11,2016-08
section HCL-Japan  (Lead Engineer)
Android-Framework(Settings) :done, des1,2016-09,2017-03
Android-Framework(SystemUI) :done, des2,2016-09,2017-03
Android-Mobile(FontVerfier) :done, des3,2016-09,2017-03
Android-TV (InitialSetup) :done, des4,2017-04,2018-01
Android-Framework(Settings) :done, des5,2018-01,2018-05
Android-Framework(SystemUI) :done, des6,2018-01,2018-05

section HCL-Japan
Android-Mobile (Bokeh) : done, des1,2018-10,2019-11
Node-Js (Tools development) : done,des1,2018-10,2019-11
DevOps (JIRA/Confluence) :active,des2,2019-11,2020-05

section Rakuten-Japan
Android-Mobile (Link) : done, des1,2019-12,2023-09

section Personal
Android-BoardGame :done, des1, 2012-06,2013-10 
Android-Library(EazyView) :done, des1, 2014-03,2014-10
Android-GLUG-Notifer : done,des1 ,2014-05,2014-09
Android-SummerCamp : done,des1,2014-12,2015-07
Android-MVPSample : done,des1,2018-06,2018-07
Android-MVVMSample(Service) : done,des1,2019-07,2019-10

ServerSetup in progress (Nginx + Node-Js ) : active,des1,2019-12,2020-05

```
<!-- End of Technical Experience GanttChart -->

